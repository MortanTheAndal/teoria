package test;
import Tools.ConjuntoPotencia;
import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.*;
import Tools.AFND;
import Tools.AFD;
import Tools.ConvertidorAFNDaAFD;
import Tools.Thompson;

public class LeConverterTest{
  private ArrayList<String> K;
  private String s;
  private String F;
  private ArrayList<ArrayList<String>> transiciones;
  private AFND automata;
  
  @Before
  public void setUp(){
    K=new ArrayList<String>();
    s=new String("inicial");
    F=new String("final");
    K.add(s);
    K.add(F);
    transiciones=new ArrayList<ArrayList<String>>();
    ArrayList<String> transicion=new ArrayList<String>();
    transicion.add(s);
    transicion.add("c");
    transicion.add(F);
    transiciones.add(transicion);
    automata=new AFND(K,s,F,transiciones);
    
  }
  @Test
  public void converterTest(){
    ConvertidorAFNDaAFD convertidor=new ConvertidorAFNDaAFD();
    AFD automata2=convertidor.AFNDtoAFD(automata);
    System.out.println(automata.K);
    System.out.println(automata.transiciones);
    System.out.println(automata2.transiciones.size());
    System.out.println(automata2.F);
    System.out.println(automata2.K.size());
    System.out.println(automata2.K);
    System.out.println(automata2.transiciones);
  }
  

}
