package test;
import Tools.ConjuntoPotencia;
import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.*;
import Tools.AFND;
import Tools.ConvertidorAFNDaAFD;
import Tools.Thompson;

public class ThompsonTest{
  private AFND automata1;
  private Thompson t=new Thompson();
  private ArrayList<ArrayList <String> > a;
  private String c;
  private String d;
  private String e;
  
  @Before
  public void setUp(){
    automata1= t.thompson1("a");
    a=new ArrayList<ArrayList <String> > ();
    c=new String("hola");
    d=new String("hola");
    e=new String("chao");
  }
  
  @Test
  public void test1(){
    assertNotNull(automata1);
    System.out.println(automata1.s);
    System.out.println(automata1.F);
    assertFalse(automata1.s==automata1.F);
    ArrayList<String> r= new ArrayList<String>();
    r.add(c);
    r.add("$");
    r.add(d);
    ArrayList<String> s= new ArrayList<String>();
    s.add(d);
    s.add("$");
    s.add(e);
    a.add(r);
    a.add(s);
    assertFalse(a.get(0).get(0)==a.get(1).get(0));
    ConvertidorAFNDaAFD convertidor=new ConvertidorAFNDaAFD();
    ArrayList<String> estados=new ArrayList<String>();
    convertidor.cadenaEpsilon(c, a, estados);
    assertTrue(estados.size()==2);
    ConjuntoPotencia prueba=new ConjuntoPotencia();
    ArrayList<ArrayList<String>> partes=prueba.lasPartes(s);
    System.out.println(partes.size());
    System.out.println(partes.get(6));
    int nConjuntosEstados=partes.size();
    ArrayList<ArrayList<String>> F= new ArrayList<ArrayList<String>>();
    for (int i=0;i<nConjuntosEstados;i++){
      int nConjuntos=partes.get(i).size();
      for (int j=0;j<nConjuntos;j++){
        if (partes.get(i).get(j)==d){
          F.add(partes.get(i));
        }
      }
    }
    System.out.println(F.size());
    System.out.println(F.get(0));
    System.out.println(F.get(1));
    System.out.println(F.get(2));
    System.out.println(F.get(3));
  }
   
}
