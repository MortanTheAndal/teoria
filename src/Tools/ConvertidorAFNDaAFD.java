package Tools;
import java.util.ArrayList;

//convierte AFDNS en AFDS segun el apunte (listo)
public class ConvertidorAFNDaAFD {
  
  public AFD AFNDtoAFD (AFND automata){
    
    ArrayList<ArrayList<String>> K=new ConjuntoPotencia().lasPartes(automata.K);
    
    ArrayList<String> s=clausuraEpsilon(automata.s, automata);
    
    ArrayList<ArrayList<String>> F=new ArrayList<ArrayList<String>>();
    int nConjuntosEstados=K.size();
    for (int i=0;i<nConjuntosEstados;i++){
      int nConjuntos=K.get(i).size();
      for (int j=0;j<nConjuntos;j++){
        if (K.get(i).get(j)==automata.F){
          F.add(K.get(i));
        }
      }
    } 
    ArrayList<ArrayList<ArrayList<String>>> transicionesAFD = new ArrayList<ArrayList<ArrayList<String>>>();
    Alfabeto alfabeto=new Alfabeto();
    Thompson T=new Thompson();
    ArrayList<String> sumidero=new ArrayList<String>();
    sumidero.add("sumidero");
    int largoAlfabeto=alfabeto.alfabeto.size();
    for (int u=0;u<largoAlfabeto;u++){
      for (int i=0;i<nConjuntosEstados;i++){

        int nEstados=K.get(i).size();
        ArrayList<ArrayList<String>> transicion=new ArrayList<ArrayList<String>>();
        transicion.add(K.get(i));
        ArrayList<String> caracter=new ArrayList<String>();
        caracter.add(alfabeto.alfabeto.get(u));
        transicion.add(caracter);
        ArrayList<String> estadoSiguiente=new ArrayList<String>();
        //Chequeamos si es estado final para hacer loop
        boolean esEstadoFinal=false;
        for(int w=0; w<F.size(); w++){
          if (K.get(i)==F.get(w)){
            esEstadoFinal=true;
          }
        }
        if (esEstadoFinal){
              estadoSiguiente=K.get(i);
        }
        else{
          for (int j=0;j<nEstados;j++){
            int nTransicionesAFND=automata.transiciones.size();
            for (int k=0; k<nTransicionesAFND; k++){
              if (K.get(i).get(j)==automata.transiciones.get(k).get(0) && alfabeto.alfabeto.get(u).equals(automata.transiciones.get(k).get(1))){
                ArrayList<String> unionIesima=clausuraEpsilon(automata.transiciones.get(k).get(2), automata);
                ArrayList<String> Union=(ArrayList<String>) T.union(estadoSiguiente, unionIesima);
                estadoSiguiente=Union;
              } 
            }
          }
        }
        if (estadoSiguiente.size()==0){
          estadoSiguiente=s;
        }
        transicion.add(estadoSiguiente);
        transicionesAFD.add(transicion);
      }
    }
    return new AFD(K,s,F,transicionesAFD);
  }
 
  public void cadenaEpsilon(String estado, ArrayList<ArrayList<String> > transiciones, ArrayList<String> conjuntoDeEstados){
    int largoTransiciones=transiciones.size();
    for (int i=0; i<largoTransiciones; i++){
      if (transiciones.get(i).get(0)==estado && transiciones.get(i).get(1).equals("$")){
        conjuntoDeEstados.add(transiciones.get(i).get(2));
        cadenaEpsilon(transiciones.get(i).get(2), transiciones, conjuntoDeEstados);
      }
    }
  }
  public ArrayList<String> clausuraEpsilon(String estado, AFND automata){
    ArrayList<String> conjuntoDeEstados =new ArrayList<String>();
    conjuntoDeEstados.add(estado);
    ArrayList<ArrayList< String > > transiciones=automata.transiciones;
    cadenaEpsilon(estado, transiciones, conjuntoDeEstados);
    return conjuntoDeEstados;
    
  }


}
