package Tools;
import java.util.Stack;
public class DoublePila {
    public Stack<String> operators;
    public Stack<AFND> automatas;
    String exp;
    Thompson T; 
    public DoublePila(String expresion){
      operators = new Stack<String>();
      automatas = new Stack<AFND>(); 
      exp=expresion;
      T= new Thompson();
    }
    public AFND generarAFND(){
      for(int i=0; i<exp.length();i++){
        switch(exp.charAt(i)){
          case '(':{
            continue;
            }
          case ')':{
            String operador=operators.pop();
            switch(operador){
              case "*":{
               AFND op=automatas.pop();
               automatas.push(T.thompson2(op, operador));
               break;
              }
              default:{
                AFND op1= automatas.pop();
                AFND op2= automatas.pop();
                automatas.push(T.thompson3(op1,op2,operador));
                break;
              }
            }
            break;
          }
          case '|':{
            operators.push("|");
            break;
          }
          case '*':{
            operators.push("*");
            break;
          }
          case '.':{
            operators.push(".");
            break;
          }
          default:{ 
            automatas.push(T.thompson1(Character.toString(exp.charAt(i))));
            break;
          }
        }
      }
      return automatas.pop();
    }
    
}
