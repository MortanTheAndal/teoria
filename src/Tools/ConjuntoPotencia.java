package Tools;
import java.util.ArrayDeque;
import java.util.ArrayList;

//Calcula las partes de una lista de strings (listo)
public class ConjuntoPotencia {

  // Stack for intermediate results.
  final ArrayDeque<String> stack = new ArrayDeque<String>();

  // Source data.
  ArrayList<String> src;

  // Powerset under construction
  ArrayList<ArrayList <String> > dst;

  // Recursive powerset calculator
  private void recur(int i) {
      if (i >= src.size()) {
          // Stack is complete. If more than 1 element,
          // add its contents to the result.
          if (stack.size() >= 1) {
              ArrayList<String> set = new ArrayList<String>();
              for (String a : stack) set.add(a);
              dst.add(set);
          }
      }
      else {
          // Otherwise recur both without and with this element
          // added to the stack.  Clean up the stack before return.
          recur(i + 1);
          stack.offerLast(src.get(i));
          recur(i + 1);
          stack.pollLast();
      }
  }

  // Get a powerset for the givens source data.
  public ArrayList<ArrayList <String>> lasPartes(ArrayList<String> src) {
      this.src = src;
      this.dst = new ArrayList<ArrayList <String>>();
      recur(0);
      return dst;
  }
}
