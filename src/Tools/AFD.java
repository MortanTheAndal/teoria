package Tools;

import java.util.ArrayList;

//clase y constructor de un AFD
public class AFD {
  
  public ArrayList<ArrayList<String>> K;
  public ArrayList<String> s;
  public ArrayList<ArrayList<String>> F;
  public ArrayList<ArrayList<ArrayList<String>>> transiciones;
  public AFD(ArrayList<ArrayList<String>> a, ArrayList<String> c, ArrayList<ArrayList<String>>  d, ArrayList<ArrayList<ArrayList<String>>> e ){
    K=a;
    s=c;
    F=d;
    transiciones=e;
  }
  public void leer(String linea){
    ArrayList<String> estadoActual=s;
    for(int i=0;i<linea.length();i++){
      String caracter=Character.toString(linea.charAt(i));
      
      //boolean existeTransicion=false;
      for(int j=0; j<transiciones.size();j++){
        ArrayList<ArrayList<String>> trans=transiciones.get(j);
        if(estadoActual==trans.get(0) && caracter.equals(trans.get(1).get(0))){
          estadoActual=trans.get(2);
          
          //existeTransicion=true;          
        }
      }
      //if(existeTransicion==false){
        //estadoActual=s;
      //}
      if(F.contains(estadoActual) ){
        System.out.println(linea);
        break;
      }
    }
  }

}
