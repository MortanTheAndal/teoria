package Tools;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class Thompson {
 public AFND thompson1(String c){
   ArrayList<String> K=new ArrayList<String>();
   String a=new String("hola");
   String b=new String("hola");
   K.add(a);
   K.add(b);
   String F=b;
   ArrayList<ArrayList<String> > transiciones=new ArrayList<ArrayList <String> >();
   ArrayList<String> d=new ArrayList<String>();
   d.add(a);
   d.add(c);
   d.add(b);
   transiciones.add(d);
   
   return new AFND(K, a, F, transiciones);
 }
  
  public AFND thompson2(AFND N, String op){
    //se asume que op es *
    ArrayList<String> Kp;
    String sp = "hola", Fp = "hola";
    ArrayList<ArrayList<String>> trans;
    ArrayList<String> temp=new ArrayList<String>(Arrays.asList(sp,Fp));
    Kp=(ArrayList<String>) union(temp,N.K);
    ArrayList<String> trans1=new ArrayList<String>(Arrays.asList(sp, "$", N.s));
    ArrayList<String> trans2=new ArrayList<String>(Arrays.asList(N.F, "$", N.s));
    ArrayList<String> trans3=new ArrayList<String>(Arrays.asList(N.F, "$", Fp));
    ArrayList<String> trans4=new ArrayList<String>(Arrays.asList(sp, "$", Fp));
    ArrayList<ArrayList<String>> newtrans= new ArrayList<ArrayList<String>>(Arrays.asList(trans1,trans2,trans3,trans4));
    trans=(ArrayList<ArrayList<String>>) union(newtrans,N.transiciones);
    return new AFND(Kp,sp,Fp,trans);
  }
  
  public AFND thompson3(AFND a, AFND b, String op){
    ArrayList<String> Kp;
    String sp, Fp;
    ArrayList<ArrayList<String>> transp;
    if(op=="|"){
      sp=new String("hola");
      Fp=new String("hola");
      ArrayList<String> temp=new ArrayList<String>(Arrays.asList(sp,Fp));
      temp=(ArrayList<String>) union(temp,a.K);
      Kp=(ArrayList<String>) union(temp,b.K);
      ArrayList<String> trans1=new ArrayList<String>(Arrays.asList(sp, "$",a.s));
      ArrayList<String> trans2=new ArrayList<String>(Arrays.asList(sp, "$",b.s));
      ArrayList<String> trans3=new ArrayList<String>(Arrays.asList(a.F, "$",Fp));
      ArrayList<String> trans4=new ArrayList<String>(Arrays.asList(b.F, "$",Fp));
      ArrayList<ArrayList<String>> newtrans= new ArrayList<ArrayList<String>>(Arrays.asList(trans1,trans2,trans3,trans4));
      newtrans=(ArrayList<ArrayList<String>>) union(newtrans,a.transiciones);
      transp=(ArrayList<ArrayList<String>>) union(newtrans,b.transiciones);
    }
    else{ // .
      Kp=(ArrayList<String>) union(a.K,b.K);
      sp=b.s;
      Fp=a.F;
      ArrayList<String> trans1= new ArrayList<String>(Arrays.asList(b.F,"$",a.s));
      ArrayList<ArrayList<String>> newtrans= new ArrayList<ArrayList<String>>(Arrays.asList(trans1));
      newtrans=(ArrayList<ArrayList<String>>) union(newtrans,a.transiciones);
      transp=(ArrayList<ArrayList<String>>) union(newtrans,b.transiciones);
    }
    return new AFND(Kp,sp,Fp,transp);
  }
  public  <T> List<T> union(ArrayList<T> list1, ArrayList<T> list2) {
    ArrayList<T> set = new ArrayList<T>();
    for(int i=0;i<list1.size();i++){
      set.add(list1.get(i));
    }
    for(int i=0;i<list2.size();i++){
      set.add(list2.get(i));
    }
    
    return (set);
  }

  public static <T> List<T> intersection(List<T> list1, List<T> list2) {
    List<T> list = new ArrayList<T>();

    for (T t : list1) {
        if(list2.contains(t)) {
            list.add(t);
        }
    }

    return list;
  }
}
