package Tarea1;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import Tools.AFD;
import Tools.AFND;
import Tools.DoublePila;
import Tools.ConvertidorAFNDaAFD;

public class Tarea1 {
  /**
   * Se corre desde consola con de la siguiente manera:
   * javac Tarea.java
   * java Tarea1 ArchivoTexto.txt "((u.n)|(n.o))" 
   */
  public static void main(String[] args) throws IOException {
    int in = args.length;
    String ErrorMsg = "Debe entregar dos argumentos. Usted ha entregado "+in+" argumentos.";
    ArrayList<String> lineasTexto;
    String expresion;
   
    if (in != 2) System.out.println(ErrorMsg);
    else {
      try {
        if (args[0].contains(".txt")) { //args[0] es texto, args[1] es expresion
          lineasTexto = almacenar(args[0]);
          expresion = args[1];
        }
        else { //inputs desordenados
          lineasTexto = almacenar(args[1]);
          expresion = args[0];          
        }
        
        DoublePila dp = new DoublePila(expresion);
        AFND N = dp.generarAFND();
        ConvertidorAFNDaAFD C = new ConvertidorAFNDaAFD();
        AFD D = C.AFNDtoAFD(N);
        
        for (String s : lineasTexto) {
          D.leer(s);
          //TODO pasar el AFD D por cada linea de texto s, si coincide entonces imprimir s
        }
        
      }
      catch (IOException e) {
        e.printStackTrace();
      }                   
    }
    
  }
  public static ArrayList<String> almacenar(String archivo) throws IOException {
    FileReader reader = new FileReader(new File(archivo)); //lector para el streaming del archivo
    BufferedReader buffer = new BufferedReader(reader);

    String linea = buffer.readLine(); //lee linea por linea del buffer
    ArrayList<String> output = new ArrayList<String>();
    while(linea != null){
        output.add(linea);
        linea = buffer.readLine();
    }
    buffer.close(); reader.close();
    return output;
  }

}
